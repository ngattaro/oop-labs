package OOP5;
/**
 * class Main khoi tao cac doi tuong
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-20
 */

public class Main
{
    public static void main (String[] args)
    {
        HoaQua fruit1 = new CamSanh(10, 3);
        HoaQua fruit2 = new CamCaoPhong(7, 1);
        HoaQua fruit3 = new QuaTao(5, 2);
        Shape shape1 = new Shape("blue",true);
        Shape shape2 = new Circle(10.5, "green", true);
        Shape shape3 = new Rectangle(2.9,7.1,"purple", false);
        Shape shape4 = new Square(5.0, "yellow", true);
        ((Square) shape4).setLength(10);
        System.out.printf(fruit1.toString());
        System.out.printf(fruit2.toString());
        System.out.printf(fruit3.toString());
        System.out.printf(shape1.toString());
        System.out.printf(shape2.toString());
        System.out.printf(shape3.toString());
        System.out.printf(shape4.toString());
    }
}
