package OOP5;
/**
 * class CamSanh Phong ke thua tu class QuaCam
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-20
 */
public class CamSanh extends QuaCam
{
    /**
     * constructor
     * @param p la price
     * @param q la quantity
     */
    public  CamSanh(int p, int q)
    {
        super(p,q,"everywhere");
    }
    /**
     * toString la ham lay thong tin cua class
     * @return String tra ve xau chua thong tin gom: quantity, price, nsx
     */
    @Override
    public String toString ()
    {
        return "quantity: " + this.quantity + " price: " + this.price + " nsx: " + this.nsx + "\n";
    }
}
