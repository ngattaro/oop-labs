package OOP5;
/**
 * class QuaCam ke thua tu class HoaQua
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-20
 */
public class QuaCam extends HoaQua
{
    protected String nsx;

    /**
     * constructor
     * @param p la price
     * @param q la quantity
     */
    public QuaCam(int p, int q, String place)
    {
        super(p,q);
        this.nsx = place;
    }
}
