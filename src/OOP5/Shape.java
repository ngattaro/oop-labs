package OOP5;
/**
 * class Shape
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-01
 */

public class Shape
{
    private String color;
    private boolean filled;

    public String getColor ()
    {
        return color;
    }

    public void setColor (String color)
    {
        this.color = color;
    }
    public boolean isFilled()
    {
        return this.filled;
    }
    public void setFilled (boolean filled)
    {
        this.filled = filled;
    }

    /**
     * Ham khoi tao mac dinh
     */
    public Shape()
    {
        color = "red";
        filled = true;
    }

    /**
     * Ham khoi tao Shape
     * @param color0 la mau sac
     * @param filled0 la trang thai fill
     */
    public Shape(String color0, boolean filled0)
    {
        setColor(color0);
        setFilled(filled0);
    }

    /**
     * Ham in ra thong tin cua class
     * @return mau cua Shape
     */
    public String toString ()
    {
        return "Color: " + getColor() + "\n";
    }
}
