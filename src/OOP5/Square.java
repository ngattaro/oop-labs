package OOP5;

/**
 * class Shape
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-01
 */

public class Square extends Rectangle
{
    public double getSide()
    {
        return getWidth();
    }
    public void setSide(double side)
    {
        setLength(side);
        setWidth(side);
    }
    @Override
    public void setLength(double side)
    {
        super.setLength(side);
        super.setWidth(side);
    }
    @Override
    public void setWidth(double side)
    {
        super.setWidth(side);
        super.setLength(side);
    }
    /**
     * Constructor rong
     */
    public Square()
    {
        super();
    }

    /**
     * Constructor
     * @param side canh cua hinh vuong
     */
    public Square(double side)
    {
        super(side,side);
    }

    /**
     * constructor
     * @param side canh cua hv
     * @param color mau sac
     * @param filled trang thai filled
     */
    public Square(double side, String color, boolean filled)
    {
        super(side,side,color,filled);
    }

    /**
     * Ham in thong tin
     * @return thong tin cua HV
     */
    @Override
    public String toString()
    {
        return "Shape: Square \nSide: " + getSide() +"\nColor: " +  getColor() + "\n";
    }
}
