package OOP5;

/**
 * class Rectangle ke thua tu class Shape
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-01
 */
public class Rectangle extends Shape
{
    private double width, length;

    public double getLength ()
    {
        return length;
    }

    public void setLength (double length)
    {
        this.length = length;
    }

    public double getWidth ()
    {
        return width;
    }

    public void setWidth (double width)
    {
        this.width = width;
    }

    /**
     * Ham khoi tao mac dinh
     */
    public Rectangle()
    {
        super();
        width = 1.0;
        length = 1.0;
    }

    /**
     * Ham khoi tao chi co chieu dai, rong
     * @param width0 chieu rong hcn
     * @param length0 chieu dai hcn
     */
    public Rectangle(double width0, double length0)
    {
        super();
        setLength(length0);
        setWidth(width0);
    }

    /**
     * Ham khoi tao day du tham so
     * @param width0 chieu rong hcn
     * @param length0 chieu dai hcn
     * @param color0 mau sac hcn
     * @param filled0 trang thai fill
     */
    public Rectangle(double width0, double length0, String color0, boolean filled0)
    {
        super(color0,filled0);
        setWidth(width0);
        setLength(length0);
    }

    /**
     * Ham tinh dien tich hcn
     * @return double dien tich hcn
     */
    public double getArea()
    {
        return getLength()*getWidth();
    }

    /**
     * Ham tinh chu vi hcn
     * @return double chu vi hcn
     */
    public double getPerimeter()
    {
        return 2*(getWidth() + getLength());
    }

    /**
     * Ham in thong tin hcn
     * @return String la thong tin gom chieu dai, rong, mau sac, loai hinh
     */
    @Override
    public String toString ()
    {
        return "Shape: Retangle\nWidth: " + getWidth() + "\nLength: " + getLength() + "\n" + super.toString();
    }
}
