package OOP5;
/**
 * class Circle ke thua tu class Shape
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-01
 */

public class Circle extends Shape
{
    private double radius;
    public static final double pi = 3.1416;
    public double getRadius ()
    {
        return radius;
    }

    public void setRadius (double radius)
    {
        this.radius = radius;
    }

    /**
     * Ham khoi tao mac dinh
     */
    public Circle()
    {
        super();
        radius = 1.0;
    }

    /**
     * Ham khoi tao chi co ban kinh
     * @param radius0 ban kinh hinh tron
     */
    public Circle(double radius0)
    {
        super();
        setRadius(radius0);
    }

    /**
     * Ham khoi tao day du thong so color, radius, filled
     * @param radius0 ban kinh hinh tron
     * @param color0 mau cua hinh tron
     * @param filled0 trang thai fill
     */
    public Circle(double radius0, String color0, boolean filled0)
    {
        super(color0,filled0);
        setRadius(radius0);
    }

    /**
     * Ham tinh dien tich hinh tron
     * @return double dien tich
     */
    public double getArea()
    {
        return pi*getRadius()*getRadius();
    }

    /**
     * Ham tinh chu vi hinh tron
     * @return double la chu vi
     */
    public double getPerimeter()
    {
        return 2*pi*getRadius();
    }

    /**
     * Ham in ra thong tin hinh tron
     * @return thong tin gom mau sac, ban kinh, hinh dang
     */
    @Override
    public String toString()
    {
        return "Shape: Circle\nRadius: " + getRadius() + "\n"+ super.toString();
    }
}
