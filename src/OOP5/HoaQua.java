package OOP5;
/**
 * class HoaQua gom cac thuoc tinh chung
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-20
 */
public class HoaQua
{
    protected int price, quantity;
    /**
     * constructor
     * @param p la price
     * @param q la quantity
     */
    public HoaQua(int p, int q)
    {
        this.price = p;
        this.quantity = q;
    }
}
