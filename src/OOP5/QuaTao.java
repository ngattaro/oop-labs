package OOP5;
/**
 * class QuaTao ke thua tu class HoaQua
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-20
 */
public class QuaTao extends HoaQua
{
    /**
     * constructor
     * @param p la price
     * @param q la quantity
     */
    public QuaTao(int p,int q)
    {
        super(p,q);
    }
    /**
     * toString la ham lay thong tin cua class
     * @return String tra ve xau chua thong tin gom: quantity, price
     */
    @Override
    public String toString ()
    {
        return "quantity: " + this.quantity + " price: " + this.price + "\n";
    }
}
