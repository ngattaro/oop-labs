package OOP8.Ex1;
/**
 * class Subtraction tru 2 bieu thuc
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

public class Subtraction extends BinaryExpression {
    private Expression left;
    private Expression right;
    /**
     * constructor
     * @param l bieu thuc ve trai
     * @param r bieu thuc ve phai
     */
    public Subtraction(Expression l, Expression r) {
        left = l;
        right = r;
    }
    @Override
    public Expression Left() { return left; }
    @Override
    public Expression Right() { return right; }
    
    public void setLeft(Expression left) {
        this.left = left;
    }
    
    public void setRight(Expression right) {
        this.right = right;
    }
    /**
     * Ham in bieu thuc
     * @return String
     */
    @Override
    public String toString() {
        return String.format("%s - %s", left, right);
    }
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public int evaluate() {
        return left.evaluate() - right.evaluate();
    }
}
