package OOP8.Ex1;
/**
 * class Square binh phuong bieu thuc
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

import static java.lang.Math.pow;

public class Square extends Expression {
    private Expression expression;

    /**
     * constructor
     * @param e bieu thuc
     */
    public Square(Expression e) { expression = e; }
    /**
     * Ham in bieu thuc
     * @return String
     */
    @Override
    public String toString() {

        return String.format("(%s)^2", expression);
    }
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public int evaluate() {
        return (int) pow(expression.evaluate(), 2);
    }
}
