package OOP8.Ex1;
/**
 * abstract class Expression la bieu thuc
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

public abstract class Expression {
    /**
     * Ham in bieu thuc
     * @return String
     */
    public abstract String toString();
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    public abstract int evaluate();
}
