package OOP8.Ex1;
/**
 * class Numeral tao so nguyen
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

public class Numeral extends Expression {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    /**
     * constructor
     */
    public Numeral(){};

    /**
     * constructor
     * @param val gia tri
     */
    public Numeral(int val)
    {
        value = val;
    }
    /**
     * Ham in bieu thuc
     * @return String
     */
    @Override
    public String toString() {
        return String.valueOf(value);
    }
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public int evaluate() {
        return value;
    }
}
