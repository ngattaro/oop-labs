package OOP8.Ex1;
/**
 * abstract class BinaryExpression cac phep toan nhi phan
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

public abstract class BinaryExpression extends Expression {
   
    public abstract Expression Left();
    public abstract Expression Right();
    
    /**
     * Ham in bieu thuc
     * @return String
     */
    @Override
    public abstract String toString();
    /**
     * Ham tinh gia tri bieu thuc
     * @return int
     */
    @Override
    public abstract int evaluate();
}
