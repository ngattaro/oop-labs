package OOP8.Ex2;
/**
 * class FileNotFound kiem tra su ton tai cua file
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileNotFound
{
    /**
     * ham doc file
     * @param path duong dan toi file
     * @throws Exception
     */
    public static void readFile(String path) throws FileNotFoundException
    {
        File file = new File(path);
        if (!file.exists()) throw new FileNotFoundException("Khong co file nay");
        Scanner scan = new Scanner(file);
        int n = scan.nextInt();
        System.out.println(n);
    }
    
    public static void main(String[] args) throws FileNotFoundException
    {
        readFile("test1.txt");
    }
}
