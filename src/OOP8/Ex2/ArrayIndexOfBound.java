package OOP8.Ex2;
/**
 * class ArrayIndexOfBound kiem tra truy cap ngoai mang
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */
import java.util.ArrayList;

public class ArrayIndexOfBound {
    /**
     * ham in ra phan tu cua mang
     * @param a mang
     * @param i thu tu
     * @throws Exception
     */
    public static void printElement(ArrayList a, int i) throws ArrayIndexOutOfBoundsException
    {
        if(i < 0 || i >= a.size()) throw new ArrayIndexOutOfBoundsException("Mang khong co phan tu thu " + i);
        System.out.println(a.get(i));
    }
    public static void main(String[] args)throws ArrayIndexOutOfBoundsException {
        ArrayList a = new ArrayList();
        a.add(10);
        printElement(a,0);
        printElement(a,1);
    }
}
