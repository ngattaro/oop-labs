package OOP8.Ex2;
/**
 * class intToBoolean kiem tra loi ep kieu
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */
public class ClassCast {
    /**
     * ham chuyen int thanh bool
     * @param n so int
     * @return gia tri sau khi chuyen
     * @throws Exception
     */
    public static boolean intToBoolean(int n) throws ClassCastException
    {
        if (n<0 || n>1) throw new ClassCastException("Ep kieu khong hop le");
        return (n==1);
    }

    public static void main(String[] args)throws ClassCastException {
        intToBoolean(1);
        intToBoolean(10);
    }
}
