package OOP8.Ex2;
/**
 * class NullPointer kiem tra con tro null
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */
public class NullPointer {
    /**
     * ham in string
     * @param s string
     * @throws Exception
     */
    public static void printString(String s) throws NullPointerException
    {
        if (s == null) throw new NullPointerException("Xau rong");
        System.out.println(s);
    }
    public static void main(String[] args)throws NullPointerException {
        printString("a");
        printString(null);
    }
}
