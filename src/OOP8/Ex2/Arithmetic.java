package OOP8.Ex2;
/**
 * class Arithmatic kiem tra loi chia cho 0
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

public class Arithmetic {
    /**
     * ham chia 2 so
     * @param a so thu 1
     * @param b so thu 2
     * @throws Exception
     */
    public static void division(int a, int b) throws ArithmeticException
    {
        if (b==0) throw new ArithmeticException("Loi chia cho 0");
        System.out.println(a/b);
    }
    public static void main(String[] args)throws ArithmeticException {
        division(10,5);
        division(10,0);
    }
}
