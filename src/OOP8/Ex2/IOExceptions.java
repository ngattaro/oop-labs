package OOP8.Ex2;
/**
 * class IOExceptions
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */
import java.io.IOException;

public class IOExceptions
{
    /**
     * Ham check IOException
     * @throws IOException
     */
    public static void checkIO() throws IOException
    {
        throw new IOException();
    }
    
    public static void main(String[] args) throws IOException
    {
        checkIO();
    }
}
