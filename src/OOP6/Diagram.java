package OOP6;
/**
 * class Diagram
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

import java.util.LinkedList;

public class Diagram
{
    protected double width, height;

    protected LinkedList<Layer> listLayers = new LinkedList<Layer>();

    public double getWidth ()
    {
        return width;
    }

    public double getHeight ()
    {
        return height;
    }

    public void setHeight (double height)
    {
        this.height = height;
    }

    public void setWidth (double width)
    {
        this.width = width;
    }

    /**
     * constructor rong
     */
    public Diagram(){};

    /**
     * constructor khoi tao chieu dai, rong
     * @param w chieu rong
     * @param h chieu dai
     */
    public Diagram(int w, int h)
    {
        setHeight(h);
        setWidth(w);
    }

    /**
     * constructor khoi tao nhieu layer
     * @param list danh sach layer
     * @param w chieu rong
     * @param h chieu dai
     */
    public Diagram(LinkedList<Layer> list, int w, int h)
    {
        setWidth(w);
        setHeight(h);
        listLayers.clear();
        listLayers.addAll(list);
    }

    /**
     * ham xoa tat ca doi tuong thuoc Retangle
     */
    public void removeAllTriangle()
    {
        for (Layer layer: listLayers)
        {
            for (Shape shape: layer.listShapes)
                if (shape instanceof Triangle) layer.listShapes.remove(shape);
        }
    }

    /**
     * Ham in thong in
     * @return thong tin Diagram
     */
    @Override
    public String toString ()
    {
        String s = "Diagram \n\nWidth: " + getWidth() + " Height: " + getHeight() + "\n\nInclude:\n\n";
        for(int i = 0; i < listLayers.size(); i++)
            s += listLayers.get(i).toString();
        return s;
    }
}
