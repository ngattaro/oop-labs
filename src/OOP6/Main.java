package OOP6;

import java.util.LinkedList;

/**
 * class Main khoi tao cac doi tuong
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-20
 */

public class Main
{
    public static void main (String[] args)
    {

        Shape shape1 = new Triangle(1.0,2.0,3.0,"blue",true,10,10);
        Shape shape2 = new Circle(10.5, "green", true,10,20);
        Shape shape3 = new Rectangle(2.9,7.1,"purple", true,30,10);
        Shape shape4 = new Square(5.0, "yellow", true,40,10);
        LinkedList<Shape> list1 = new LinkedList<Shape>();
        LinkedList<Shape> list2 = new LinkedList<Shape>();
        list1.add(shape1);
        list1.add(shape2);
        list2.add(shape3);
        list2.add(shape4);
        Layer layer1 = new Layer(list1,100,100);
        Layer layer2 = new Layer(list1,100,300);
        LinkedList<Layer> list3 = new LinkedList<Layer>();
        list3.add(layer1);
        list3.add(layer2);
        Diagram diagram = new Diagram(list3,400,400);
        System.out.printf(shape1.toString());
        System.out.printf(shape2.toString());
        System.out.printf(shape3.toString());
        System.out.printf(shape4.toString());
        System.out.println("Layer1:\n");
        System.out.println(layer1.toString());
        System.out.println("Layer2:\n");
        System.out.println(layer1.toString());
        System.out.println(diagram.toString());
        layer1.removeAllCircle();
        System.out.println("Layer1:\n");
        System.out.println(layer1.toString());
        diagram.removeAllTriangle();
        System.out.println(diagram.toString());
    }
}
