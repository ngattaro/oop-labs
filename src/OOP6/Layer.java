package OOP6;
/**
 * class Layer ke thua tu class Diagram
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

import java.util.LinkedList;

public class Layer extends Diagram
{
    protected int  layoutX, layoutY;
    protected LinkedList<Shape> listShapes = new LinkedList<Shape>();

    public void setLayoutY (int layoutY)
    {
        this.layoutY = layoutY;
    }

    public void setLayoutX (int layoutX)
    {
        this.layoutX = layoutX;
    }

    public int getLayoutY ()
    {
        return layoutY;
    }

    public int getLayoutX ()
    {
        return layoutX;
    }

    /**
     * constructor rong
     */
    public Layer(){};

    /**
     * constructor khoi tao chieu dai, rong
     * @param w chieu rong
     * @param h chieu dai
     */
    public Layer(int w, int h)
    {
        super(w,h);
    };

    /**
     * constructor khoi tao vi tri, chieu dai, rong
     * @param w chieu rong
     * @param h chieu dai
     * @param x toa do x
     * @param y toa do y
     */
    public Layer(int w, int h, int x, int y)
    {
        super(w,h);
        setLayoutX(x);
        setLayoutY(y);
    }

    /**
     * constructor khoi tao layer gom nhieu shape
     * @param list danh sach cac shape
     * @param w chieu rong
     * @param h chieu dai
     */
    public Layer(LinkedList<Shape> list, int w, int h)
    {
        super(w,h);
        listShapes.clear();
        listShapes.addAll(list);
    }

    /**
     * ham xoa cac doi tuong thuoc class Circle
     */
    public void removeAllCircle()
    {
        for (Shape shape: listShapes)
            if (shape instanceof Circle) listShapes.remove(shape);

    }

    /**
     * ham in thong tin
     * @return tra ve thong tin cua lop layer
     */
    @Override
    public String toString ()
    {
        String s = "Layer \n\nPosition: " + getLayoutX() + " " + getLayoutY() + "\n";
        s += "Width: " + getWidth() + " Height: " + getHeight() + "\n\nInclude:\n\n";
        for(int i = 0; i < listShapes.size(); i++)
            s += listShapes.get(i).toString();
        return s;

    }
}

