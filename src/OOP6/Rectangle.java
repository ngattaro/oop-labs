package OOP6;

/**
 * class Rectangle ke thua tu class Shape
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */
public class Rectangle extends Shape
{
    /**
     * Ham khoi tao mac dinh
     */
    public Rectangle()
    {
        super();
        setWidth(1.0);
        setHeight(1.0);
    }

    /**
     * Ham khoi tao chi co chieu dai, rong
     * @param width0 chieu rong hcn
     * @param length0 chieu dai hcn
     */
    public Rectangle(double width0, double length0)
    {
        super();
        setHeight(length0);
        setWidth(width0);
    }

    /**
     * Ham khoi tao day du tham so
     * @param width0 chieu rong hcn
     * @param length0 chieu dai hcn
     * @param color0 mau sac hcn
     * @param filled0 trang thai fill
     * @param layoutX toa do x
     * @param layoutY toa do y
     */
    public Rectangle(double width0, double length0, String color0, boolean filled0, int layoutX, int layoutY)
    {
        super(color0,filled0,layoutX,layoutY);
        setWidth(width0);
        setHeight(length0);

    }

    /**
     * Ham tinh dien tich hcn
     * @return double dien tich hcn
     */
    public double getArea()
    {
        return getHeight()*getWidth();
    }

    /**
     * Ham tinh chu vi hcn
     * @return double chu vi hcn
     */
    public double getPerimeter()
    {
        return 2*(getWidth() + getHeight());
    }

    /**
     * Ham in thong tin hcn
     * @return String la thong tin gom chieu dai, rong, mau sac, loai hinh
     */
    @Override
    public String toString ()
    {
        return "Shape: Retangle\nWidth: " + getWidth() + "\nLength: " + getHeight() + "\n" + super.toString();
    }
}
