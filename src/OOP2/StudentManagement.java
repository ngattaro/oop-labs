package OOP2;
/**
 * class StudentManagement quan ly cac sinh vien( them sv, xoa sv, in ds sinh vien theo group)
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-11
 */
import OOP2.Student;

public class StudentManagement
{
    // mang students luu danh sach cac sinh vien
    private Student[] students = new Student[100];

    // bien countStudent dem so long sinh vien dang co trong danh sach
    public  int countStudent = 0;

    /**
     * ham sameGroup de so sanh 2 sinh vien co thuoc cung mot nhom hay khong
     * @param s1 la sinh vien 1
     * @param s2 la sinh vien 2
     */
    public static boolean sameGroup(Student s1, Student s2)
    {
        return s1.getGroup()==s2.getGroup();
    }
    /**
     * studentByGroup la ham in ra danh sach sinh vien theo group
     */
    public void studentsByGroup()
    {
        // mang ok danh dau xem student da duoc xet hay chua
        boolean[] ok = new boolean[countStudent];
        // khoi tao mang ok ban dau la true
        for(int i = 0; i < countStudent; i++) ok[i] = true;
        for(int i = 0; i < this.countStudent; i++)
            if (ok[i])
            {
                //neu students[i] chua dc xet thi xac nhan day la sv dau tien cua group nay va in ra ten group
                System.out.println(this.students[i].getGroup());
                for (int j = i; j < this.countStudent; j++)
                    if (sameGroup(this.students[i],this.students[j]))
                    {
                        // in ra cac sv thuoc group nay va danh dau la da xet
                        ok[j] = false;
                        this.students[j].getInfor();
                    }
            }
    }
    /**
     * ham removeStudent de xoa mot sinh vien ra khoi danh sach
     * @param sid la mssv cua sinh vien do
     */
    public  void removeStudent(String sid)
    {
        int i;
        for (i = 0; i < this.countStudent-1; i++)
            if (this.students[i].getId()==sid) break;
        for(int j = i; i <this.countStudent; j++) this.students[i] = this.students[i+1];
        countStudent--;
    }
    /**
     * ham addStudent de them mot sinh vien vao danh sach
     * @param s la sinh vien do
     */
    public  void addStudent(Student s)
    {
        this.students[this.countStudent] = new Student();
        this.students[this.countStudent] = s;
        countStudent++;

    }

    public static void main(String[] args)
    {
        Student a = new Student("Ngat", " 17020061", "ngattaro@gmail.com");
        StudentManagement p = new StudentManagement();
        a.setGroup("K59CLC");
        p.addStudent(a);
        a.setName("ABC");
        p.addStudent(a);
        a.setGroup("K59CB");
        p.addStudent(a);
        p.studentsByGroup();

    }

}