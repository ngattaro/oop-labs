package OOP2;
/**
 * class Student quan ly sinh vien
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-11
 */
public class Student
{
    private String name, id, group, email;
    public String getName()
    {
        return name;
    }

    public String getId()
    {
        return id;
    }

    public String getEmail()
    {
        return email;
    }

    public String getGroup()
    {
        return group;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }
    /**
     * ham getInfor in ra thong tin cua sinh vien
     */
    public void getInfor()
    {
        System.out.printf("Ho ten: %s\n", this.name);
        System.out.printf("MSSV: %s\n", this.id);
        System.out.printf("Group: %s\n", this.group);
        System.out.printf("Email: %s\n", this.email);
    }
    /**
     * ham Student() khoi tao mac dinh khi khong co tham so truyen vao
     */
    public Student()
    {
        setName("Student");
        setId("000");
        setGroup("K59CB");
        setEmail("uet@vnu.edu.vn");
    }
    /**
     * ham Student(n,sid,em) khoi tao khi biet ten, mssv, email
     * @param n la ten sinh vien
     * @param sid la mssv cua sinh vien
     * @param em la email cua sinh vien
     */
    public Student(String n, String sid, String em)
    {
        setName(n);
        setId(sid);
        setGroup("K59CB");
        setEmail(em);
    }
    /**
     * ham Student(s) khoi tao sao chep
     * @param s la sinh vien can sao chep
     */
    public Student(Student s)
    {
        setName(s.getName());
        setId(s.getId());
        setGroup(s.getGroup());
        setEmail(s.getEmail());
    }
}
