package OOP3;

public class Teacher
{
    private String name, sex, major;
    private int height, weight, old;

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public void setWeight(int weight)
    {
        this.weight = weight;
    }

    public void setOld(int old)
    {
        this.old = old;
    }

    public int getWeight()
    {
        return weight;
    }

    public int getOld()
    {
        return old;
    }

    public String getMajor()
    {
        return major;
    }

    public String getSex()
    {
        return sex;
    }

    public void setMajor(String major)
    {
        this.major = major;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

}
