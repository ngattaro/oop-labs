package OOP3;
/**
 * chuong trinh tim so fibonacci thu n
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-18
 */
import java.util.Scanner;

public class Fibonacci
{
    /**
    * fibo la phuong thuc tinh so fibonacci thu n
    * @param n la so can tinh so fibo
    * @return int phuong thuc fibo tra ve F[n] la so fibo thu n
    */
    public int fibo(int n)
    {
        if (n<=1) return n;
        return fibo(n-1)+fibo(n-2);
    }
    public static void main(String[] args)
    {
        System.out.printf("Nhap vao so n: ");
        Scanner sc = new Scanner(System.in);
        Fibonacci Ob = new Fibonacci();
        int n,fn;
        n = sc.nextInt();
	if (n<0) System.out.printf("Khong co so fibonacci thu %d",n);
	else
	{
        	fn = Ob.fibo(n);
        	System.out.printf("So fibonacci thu %d la: %d",n,fn);
	}    
    }
}
