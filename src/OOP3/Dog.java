package OOP3;

public class Dog
{
    private String type;
    private int height, weight, old;

    public int getHeight()
    {
        return height;
    }

    public int getOld()
    {
        return old;
    }

    public int getWeight()
    {
        return weight;
    }

    public String getType()
    {
        return type;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public void setOld(int old)
    {
        this.old = old;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public void setWeight(int weight)
    {
        this.weight = weight;
    }
}
