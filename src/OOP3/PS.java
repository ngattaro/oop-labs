package OOP3;
/**
 * chuong trinh thao tac tren phan so(cong, tru, nhan, chia, so sanh bang)
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-18
 */
public class PS
{
    private int tu,mau;

    public void setTu(int x)
    {
        this.tu = x;
    }

    public void setMau(int mau)
    {
        this.mau = mau;
    }

    public int getTu()
    {
        return this.tu;
    }

    public int getMau()
    {
        return this.mau;
    }
    /**
     * PS la phuong thuc khoi tao phan so
     * @param x la tu so
     * @param y la mau so
     */
    public void PS(int x, int y)
    {
        this.setMau(y);
        this.setTu(x);
    }
    /**
    * simplify la phuong thuc toi gian 1 phan so
    */
    public void simplify()
    {
        GCD Ob = new GCD();
        int a = this.getTu();
        int b = this.getMau();
        if((a<0 && b<0) || (a>0 && b <0))
        {
            a = -a; b= -b;
        }
        int d = Ob.gcd(this.getMau(),this.getTu());
        this.setTu(a/d);
        this.setMau(b/d);
    }
    /**
     * add la ham cong 2 phan so
     * @param a la phan so duoc cong them
     * @return PS tra ve phan so ket qua la tong 2 phan so
     */
    public PS add(PS a)
    {
        a.setTu(a.getTu()*this.getMau()+this.getTu()*a.getMau());
        a.setMau(a.getMau()*this.getMau());
        a.simplify();
        return a;
    }
    /**
    * sub la ham tru 2 phan so
    * @param a la phan so tru
    * @return PS tra ve phan so ket qua la hieu 2 phan so
    */
    public PS sub(PS a)
    {
        a.setTu(-a.getTu()*this.getMau()+this.getTu()*a.getMau());
        a.setMau(a.getMau()*this.getMau());
        a.simplify();
        return a;
    }
    /**
    * mul la ham nhan 2 phan so
    * @param a la phan so duoc nhan
    * @return PS tra ve phan so ket qua la tich 2 phan so
    */
    public PS mul (PS a)
    {
        a.setTu(a.getTu()*this.getTu());
        a.setMau(a.getMau()*this.getMau());
        a.simplify();
        return a;
    }
    /**
    * div la ham chia 2 phan so
    * @param a la phan so chia
    * @return PS tra ve phan so ket qua la thuong 2 phan so
    */
    public PS div (PS a)
    {
        int x = a.getMau()*this.getTu();
        a.setMau(a.getTu()*this.getMau());
        a.setTu(x);
        a.simplify();
        return a;
    }
    /**
    * equals la ham kiem tra 2 phan so bang nhau
    * @param a la phan so can kiem tra
    * @return boolean tra ve so ket qua bang hoac ko bang cua 2 phan so
    */
    public boolean equals(PS a)
    {
        a.simplify();
        this.simplify();
        return (a.getTu()==this.getTu() && a.getMau()==this.getMau());
    }
    /**
    * print la ham in phan so
    */
    public void print()
    {
        System.out.printf("%d/%d\n",this.getTu(),this.getMau());
    }


    public static void main(String[] args)
    {
        PS a = new PS();
        PS b = new PS();
        PS c = new PS();
        a.PS(1,2);
        b.PS(-1,-2);
        c = a.add(b);
        c.print();
	c = a.sub(b);
	c.print();
	c = a.mul(b);
	c.print();
	c = a.div(b);
	c.print();
    }
}
