package OOP3;

public class Student
{
    private String name,id, email,group;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getGroup()
    {
        return group;
    }

    public String getEmail()
    {
        return email;
    }

    public String getId()
    {
        return id;
    }
}
