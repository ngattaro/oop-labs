package OOP3;
/**
 * chuong trinh tim UCLN cua 2 so
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-18
 */
import java.util.Scanner;

public class GCD
{
    	 /**
    	 * gcd la phuong thuc tinh UCLN cua 2 so nguyen
	 * @param a la so nguyen thu nhat
	 * @param b la so nguyen thu hai
	 * @return int phuong thuc gcd tra ve UCLN(a,b)
	 */
	 public  int gcd(int a, int b)
	 {
		if (a==b || a==-b) return Math.abs(a);
		if (a*b==0) return Math.abs(a+b);
		return gcd(a%b,b%a);
	 }

    	 public static void main(String[] args)
    	 {
		System.out.printf("Nhap vao 2 so can tinh UCLN: ");
		Scanner sc = new Scanner(System.in);
		int a, b, c;
		GCD Ob = new GCD();
		a = sc.nextInt();
		b = sc.nextInt();
		c = Ob.gcd(a,b);
		System.out.printf("GCD(%d, %d) = %d \n" ,a,b,c);

    	}
}
