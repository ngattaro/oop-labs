package OOP12.Ex2;
/**
 * class Main chay thu chuong trinh
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-21
 */
public class Main
{
    public static void main(String[] args)
    {
        Array a = new Array();
        a.addElement(10);
        a.addElement(1);
        a.addElement(4);
        a.addElement(9);
        a.addElement(0);
        a.addElement(3);
        a.addElement(7);
        SortAlgorithm bubbleSort = new BubbleSort();
        SortAlgorithm selectionSort = new SelectionSort();
        //a.setSortAlgorithm(bubbleSort);
        a.setSortAlgorithm(selectionSort);
        a.printArray();
        a.sort(true);
        a.printArray();
    }
    
}
