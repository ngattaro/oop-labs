package OOP12.Ex2;
/**
 * class SelectionSort cai dat ham sort
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-21
 */
public class SelectionSort implements SortAlgorithm
{
    
    /**
     * Cai dat lai ham sort
     * @param a mang so nguyen
     * @param n so luong phan tu cua mang
     * @param ascending T/F sx tang dan hay giam dan
     * @return mang da sx
     */
    @Override
    public int[] sort(int[] a, int n, boolean ascending)
    {
        for(int i = 0; i < n; i++)
        {
            int lj = i;
            for(int j = i+1; j < n; j++)
                if((a[lj]>a[j]) == ascending)
                {
                    lj = j;
                }
            int tmp = a[i];
            a[i] = a[lj];
            a[lj] = tmp;
        }
        
        return a;
    }
}
