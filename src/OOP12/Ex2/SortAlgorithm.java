package OOP12.Ex2;
/**
 * interface SortAlgorithm co ham sort
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-21
 */
public interface SortAlgorithm
{
    /**
     * Ham sort
     * @param a mang so nguyen
     * @param n so luong phan tu cua mang
     * @param ascending T/F sx tang dan hay giam dan
     * @return mang da sx
     */
    public int[] sort(int[] a, int n, boolean ascending);
}
