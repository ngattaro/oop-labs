package OOP12.Ex2;

/**
 * class Array quan ly mang so nguyen
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-21
 */
public class Array
{
    private SortAlgorithm sortAlgorithm;
    private int[] array;
    private int size = 0;
    
    public Array()
    {
        size = 0;
        array = new int[100000];
    }
    
    public int getSize()
    {
        return size;
    }
    
    public void setSize(int size)
    {
        this.size = size;
    }
    
    public void setArray(int[] array)
    {
        this.array = array;
    }
    
    
    public void setSortAlgorithm(SortAlgorithm sortAlgorithm)
    {
        this.sortAlgorithm = sortAlgorithm;
    }
    
    /**
     * Ham them mot phan tu vao cuoi mang
     * @param v gia tri phan tu
     */
    public void addElement(int v)
    {
        array[size] = v;
        size++;
    }
    
    /**
     * Ham sort mang
     * @param ascending T/F sap xep tang dan hay giam dan
     */
    public void sort(boolean ascending)
    {
        array = sortAlgorithm.sort(array, size, ascending);
    }
    
    /**
     * Ham in mang
     */
    public void printArray()
    {
        for (int i = 0; i < size; i++) System.out.print(array[i] + " ");
        System.out.println();
    }
}
