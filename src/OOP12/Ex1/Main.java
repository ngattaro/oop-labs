package OOP12.Ex1;

/**
 * class Main kiem tra
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-20
 */
public class Main
{
    /**
     * Ham in ra nhung nguoi ko ket hon
     * @param person nut goc
     */
    public static void printLeaf(Person person)
    {
        if (person instanceof Children || (person instanceof Parents && ((Parents) person).list.isEmpty()))
        {
            System.out.println(person.name);
            return;
        }
        for (Person p : ((Parents) person).list) printLeaf(p);
    }
    
    /**
     * Ham in cap vo chong co 2 con
     * @param person nut goc
     */
    public static void printParent(Person person)
    {
        if (person instanceof Children || (person instanceof Parents && ((Parents) person).list.isEmpty())) return;
        if(((Parents) person).list.size() == 2) System.out.println(person.name + " " +((Parents) person).marriedWith);
        for (Person p : ((Parents) person).list) printParent(p);
    }
    
    /**
     * Ham in the he moi nhat
     * @param person nut goc
     * @param latest check xem nut hien tai co kha nang lam nut moi nhat ko(F: chac chan ko, T: co the co)
     */
    public static void printLatest(Person person, boolean latest)
    {
       if(person instanceof Children)
       {
           if (latest) System.out.println(person.name);
           return;
       }
       boolean check = true;
       for (Person p : ((Parents) person).list)
       {
            if (p instanceof Parents && ((Parents) p).list.size()>0) check = false;
       }
       for (Person p : ((Parents) person).list)
       {
        printLatest(p,check);
       }
    }
    
    public static void main(String[] args)
    {
        Parents grandfather = new Parents("James", "1/1/1954", "Male", "Hanna");
        grandfather.addChildren(new Children("Ryan", "10/1/1988","Male"));
        Parents father = new Parents("Kai","10/2/1990","Male","Jennifer");
        grandfather.addChildren(father);
        Parents child1 = new Parents("Tom", "1/1/2018","Male","Jerry");
        child1.addChildren(new Children("Mouse","1/1/2040","Female"));
        father.addChildren(child1);
        Parents child2 = new Parents("John", "1/1/2019","Male","Marry");
        child2.addChildren(new Children("Linn","1/1/2041","Female"));
        father.addChildren(child2);
        Parents child3 = new Parents("Clara", "1/1/2018","Female","Den");
        child3.addChildren(new Children("Elysia","1/1/2040","Female"));
        child3.addChildren(new Children("Louis","1/3/2045","Male"));
        father.addChildren(child3);
        Parents child4 = new Parents("Sarah", "1/1/2018","Female","Paul");
        child4.addChildren(new Children("Jena","1/1/2040","Male"));
        father.addChildren(child4);
        grandfather.display(1);
        System.out.println("Doesn't marry:");
        printLeaf(grandfather);
        System.out.println("Couple with two children");
        printParent(grandfather);
        System.out.println("Latest generation");
        printLatest(grandfather,true);
    }
}
