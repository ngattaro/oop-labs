package OOP12.Ex1;
/**
 * class Person
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-20
 */
public abstract class Person
{
    protected String name, dateOfBirth, sex;
    /**
     * constructor
     * @param name ten
     * @param dateOfBirth ngay thang nam sinh
     * @param sex gioi tinh
     */
    public Person(String name, String dateOfBirth, String sex)
    {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.sex = sex;
    }
    /**
     * Ham in pha he
     * @param depth do sau
     */
    public abstract void display(int depth);
}
