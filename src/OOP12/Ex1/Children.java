package OOP12.Ex1;
/**
 * class Children
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-20
 */

import java.util.Arrays;

public class Children extends Person
{
    /**
     * constructor
     * @param name ten
     * @param dateOfBirth ngay thang nam sinh
     * @param sex gioi tinh
     */
    public Children(String name, String dateOfBirth, String sex)
    {
        super(name, dateOfBirth, sex);
    }
    
    /**
     * Ham in pha he
     * @param depth do sau
     */
    @Override
    public void display(int depth)
    {
        char[] charArray = new char[depth];
        Arrays.fill(charArray, '-');
        System.out.println(new String(charArray) + super.name);
    }
}
