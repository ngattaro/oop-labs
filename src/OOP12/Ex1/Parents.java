package OOP12.Ex1;
/**
 * class Parents
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-20
 */

import java.util.Arrays;
import java.util.LinkedList;

public class Parents extends Person
{
    protected String marriedWith;
    protected LinkedList<Person> list = new LinkedList<Person>();
    /**
     * constructor
     * @param name ten
     * @param dateOfBirth ngay thang nam sinh
     * @param sex gioi tinh
     */
    public Parents(String name, String dateOfBirth, String sex)
    {
        super(name, dateOfBirth, sex);
    }
    /**
     * constructor
     * @param name ten
     * @param dateOfBirth ngay thang nam sinh
     * @param sex gioi tinh
     * @param marriedWith vo/chong
     */
    public Parents(String name, String dateOfBirth, String sex, String marriedWith)
    {
        super(name, dateOfBirth, sex);
        this.marriedWith = marriedWith;
    }
    
    /**
     * Ham them mot con
     * @param p person
     */
    public void addChildren(Person p)
    {
        list.add(p);
    }
    /**
     * Ham in pha he
     * @param depth do sau
     */
    @Override
    public void display(int depth) {
        char[] charArray = new char[depth];
        Arrays.fill(charArray, '-');
        System.out.println(new String(charArray) + super.name);
        for(Person c : list){
            c.display(depth + 2);
        }
    }
    
}
