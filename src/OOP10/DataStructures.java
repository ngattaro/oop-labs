package OOP10;
/**
 * class DataStructures
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-21
 */
import OOP9.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class DataStructures
{
    private static final File file = new File("src/OOP9/Utils.java");
    
    /**
     * Ham dem so luong ngoac trong xau s
     * @param s xau s
     * @return so luong ngoac mo - sl ngoac dong
     */
    public static int countBracket(String s)
    {
        int r = 0;
        int vt = -1;
        while(vt < s.length())
        {
            vt = s.indexOf('{',vt+1);
            if(vt != -1) r++;else break;
            
        }
        vt = -1;
        while(vt <s.length())
        {
            vt = s.indexOf('}',vt+1);
            if(vt != -1) r--;else break;
        }
        return r;
        
    }
    
    /**
     * Ham lay tat ca ham co name o trong ten
     * @param name phan co trong khai bao ham
     * @param path dong dan toi file
     * @return list cac ham
     * @throws IOException
     */
    public static List<String> getAllFunctions(String name, File path) throws IOException
    {
        Utils utils = new Utils();
        String content = utils.readContentFromFile(path.getPath());
        List<String> list = new LinkedList<String>();
        List<Integer> index = new LinkedList<Integer>();
        content += '\n';
        int start = 0;
        int end;
        while (start < content.length())
        {
            end = content.indexOf('\n',start);
            String tmp = content.substring(start,end);
            start = end+1;
            if(tmp.contains(name))
            {
                String function = tmp+"\n";
                index.add(new Integer(start-tmp.length()));
                int dem = countBracket(tmp);
                if(!tmp.contains("{") || dem != 0)
                {
                    while (start < content.length())
                    {
                        end = content.indexOf('\n', start);
                        tmp = content.substring(start, end);
                        start = end + 1;
                        function += tmp + "\n";
                        dem += countBracket(tmp);
                        if(dem == 0) break;
                    }
                }
                list.add(function);
            }
        }
        
        start = content.indexOf("/*",0);;
        int vt = 0;
        while (start <content.length())
        {
            end = content.indexOf("*/",start+2);
            while (index.get(vt).intValue()+4<start) vt++;
            if(index.get(vt).intValue()+4>=start && index.get(vt).intValue()+list.get(vt).length()-4<= end)
            {
                while (vt<list.size() && index.get(vt).intValue()+list.get(vt).length()-4<=end)
                {
                    index.set(vt,new Integer(-1));
                    vt++;
                }
            }
            start = content.indexOf("/*",end+2);
            if (start == -1) break;
        }
        
        for(int i = list.size()-1;i>=0;i--)
            if(index.get(i).intValue() == -1) list.remove(i);
        return list;
    
    }
    
    /**
     * Ham tim noi dung ham theo ten
     * @param name ten ham
     * @return noi dung ham
     * @throws IOException
     */
    public static String findFunctionByName(String name)throws IOException
    {
       
        String subname = name.substring(0,name.indexOf('(',0)-1);
        name = name.substring(name.indexOf("(")+1, name.indexOf(")"));
        name.trim();
        String[] a = name.split(",");
        List<String> list = getAllFunctions(subname, file);
        for(String s :list)
        {
            int start = s.indexOf('(',0);
            int end = s.indexOf(')',0);
            String s2 = s.substring(s.indexOf("(")+1, s.indexOf(")"));
            s2.trim();
            String[] b = s2.split(",");
            if(a.length != b.length) continue;
            boolean check = true;
            for(int j = 0; j <a.length; j++)
            {
                if (!b[j].trim().startsWith(a[j].trim()))
                {
                    check = false;
                    break;
                }
            }
            if(!check) continue;
            return s;
        }
        return null;
        
    }
    public static void main(String[] args)throws IOException
    {
        List<String> list = new ArrayList<String>();
        list = (List<String>) getAllFunctions("static",file);
        for(String s :list) System.out.println(s);
        String contentFunction = findFunctionByName("public static File findFileByName(String, String)");
        System.out.println(contentFunction);
    }
}
