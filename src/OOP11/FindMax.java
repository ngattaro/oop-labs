package OOP11;

import java.util.ArrayList;

public class FindMax
{
    public static <T extends Comparable> T max(ArrayList<T> arr)
    {
        if(arr==null || arr.size()==0) return null;
        T result = arr.get(0);
        for (T element : arr)
        {
            if(result.compareTo(element)<0) result = element;
        }
        return result;
    }
    
    public static void main(String[] args)
    {
        ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(0); arr.add(9); arr.add(2); arr.add(7); arr.add(10);
        System.out.println(max(arr));
        ArrayList<Double> arr1 = new ArrayList<Double>();
        arr1.add(0.5); arr1.add(9.1); arr1.add(4.2); arr1.add(7.2); arr1.add(9.12);
        System.out.println(max(arr1));
    }
}
