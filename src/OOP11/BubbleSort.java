package OOP11;

public class BubbleSort
{
    public static <T extends Comparable> void sort(T[] arr)
    {
        for(int i = 0; i<arr.length; i++)
            for(int j =i+1; j <arr.length;j++)
            if(arr[i].compareTo(arr[j])>0)
            {
                T tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
            }
    }
    public static <T> void print(T[] arr)
    {
        for(T element : arr)
            System.out.print(element + " ");
        System.out.println();
    }
    
    public static void main(String[] args)
    {
        Integer[] arr = {8,2,5,1,5,9,2};
        print(arr);
        sort(arr);
        print(arr);
        Double[] arr2 = {8.9,0.2,5.6,9.1,4.5,9.0,3.2};
        print(arr2);
        sort(arr2);
        print(arr2);
        String[] arr1 = {"Ngat", "Hoa", "Tung", "Ly", "Thao"};
        print(arr1);
        sort(arr1);
        print(arr1);
    }
}
