package OOP9;
/**
 * class Utils gom mot so thao tac voi file
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

import java.io.*;
import java.util.Scanner;

public class Utils
{
    /*public static void test(){}*/
    /**
     * Doc file
     * @param path duong dan toi file
     * @return noi dung file
     * @throws IOException
     */
    public static String readContentFromFile(String path) throws IOException
    {
        String s = "";
        File file = new File(path);
        Scanner scanner = new Scanner(file);
        while (scanner.hasNextLine()) s += scanner.nextLine()+"\n";
        return s;
    }
    /*
    
    
    public static void test(){}
    */
    /**
     * Tao file moi va viet vao file
     * @param path duong dan toi file
     * @throws IOException
     */
    public static void writeContentToFile(String path) throws IOException
    {
        FileWriter fileWriter = new FileWriter(path);
        fileWriter.write("mot\nhai\nba\n");
        fileWriter.close();
    }
    
    /**
     * Ham viet bo sung them vao file
     * @param path duong dan toi file
     * @throws IOException
     */
    public static void writeContentToFile2(String path) throws IOException
    {
        FileWriter fileWriter = new FileWriter(path,true);
        fileWriter.write("bon\nnam\nsau\n");
        fileWriter.close();
    }
    public static File findFileByName(int v, String u) {return null;}
    /**
     * Ham tim file
     * @param folderPath thu muc can tim
     * @param fileName ten file can tim
     * @return file
     */
    public static File findFileByName(String folderPath, String fileName)
    {
        File folder = new File(folderPath);
        File[] listFile = folder.listFiles();
        for (File file :listFile)
            if (file.getName().equals(fileName)) return file;
            //System.out.println(file.getName()+"\n");
            return null;
    }
    public static void main(String[] args) throws IOException
    {
        String s = readContentFromFile("test.txt");
        System.out.println(s);
        writeContentToFile("test.txt");
        writeContentToFile2("test.txt");
        File file = findFileByName("src/OOP7","Circle.java");
        if (file!=null) System.out.println(file.getAbsolutePath());
        else System.out.println("file doesn't exist");
    }
}
