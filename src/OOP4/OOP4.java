package OOP4;
/**
 * chuong trinh tim max(a,b), min cua Array, tinh Bmi
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-20
 */
public class OOP4 {

    /**
     * max la ham tim max cua 2 so nguyen
     * @param a la so nguyen 1
     * @param b la so nguyen 2
     * @return int tra ve so lon nhat trong 2 so a,b
     */
    public int max(int a, int b)
    {
        return (a>b) ? a : b;
    }
    /**
     * minArr la ham tim min trong 1 mang
     * @param n la so phan tu cua mang
     * @param a la mang can tim min
     * @return int tra ve so nho nhat trong mang
     */
    public int minArr(int n, int[] a)
    {
        int res = a[0];
        for(int p : a)
            if (p < res) res = p;
            return res;
    }
    /**
     * checkBmi la ham tinh chi so Bmi roi nhan xet
     * @param weight la can nang
     * @param  height la chieu cao
     * @return String tra ve nhan xet voi tung chi so bmi
     */
    public String checkBmi(double weight, double height)
    {
         double bmi = weight/(height*height);
         if (bmi < 18.5) return "Thieu can";
         if (bmi < 22.99) return "Binh thuong";
         if (bmi < 24.99) return "Thua can";
         return "Beo phi";
    }

}
