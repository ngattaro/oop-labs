package OOP7;
/**
 * class Shape ke thua tu class Layer
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

public class Shape extends Layer
{
    private String color;
    private boolean filled;

    public String getColor ()
    {
        return color;
    }

    public void setColor (String color)
    {
        this.color = color;
    }
    public boolean isFilled()
    {
        return this.filled;
    }
    public void setFilled (boolean filled)
    {
        this.filled = filled;
    }

    /**
     * Ham khoi tao mac dinh
     */
    public Shape()
    {
        color = "red";
        filled = true;
    }

    /**
     * Ham khoi tao Shape
     * @param color0 la mau sac
     * @param filled0 la trang thai fill
     * @param  x la toa do x
     * @param y la toa do y
     */
    public Shape(String color0, boolean filled0, int x, int y )
    {
        setColor(color0);
        setFilled(filled0);
        setLayoutX(x);
        setLayoutY(y);
    }
    /**
     * Ham in ra thong tin cua class
     * @return mau cua Shape
     */
    public String toString ()
    {
        return "Position: " + getLayoutX() + " " + getLayoutY() + "\nIsfilled: " + isFilled() + " Color: "+ getColor() + "\n\n" ;
    }
}
