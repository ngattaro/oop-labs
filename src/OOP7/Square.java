package OOP7;

/**
 * class Square ke thua Class Retangle
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

public class Square extends Rectangle
{
    public double getSide()
    {
        return getWidth();
    }
    public void setSide(double side)
    {
        setHeight(side);
        setWidth(side);
    }
    @Override
    public void setHeight(double side)
    {
        super.setHeight(side);
        super.setWidth(side);
    }
    @Override
    public void setWidth(double side)
    {
        super.setWidth(side);
        super.setHeight(side);
    }
    /**
     * Constructor rong
     */
    public Square()
    {
        super();
    }

    /**
     * Constructor
     * @param side canh cua hinh vuong
     */
    public Square(double side)
    {
        super(side,side);
    }

    /**
     * constructor
     * @param side canh cua hv
     * @param color mau sac
     * @param filled trang thai filled
     */
    public Square(double side, String color, boolean filled, int x, int y)
    {
        super(side,side,color,filled, x, y);
    }

    /**
     * Ham in thong tin
     * @return thong tin cua HV
     */
    @Override
    public String toString()
    {
        return "Shape: Square \nSide: " + getSide() +"\nPosition: " + getLayoutX() + " "+getLayoutY()+"\nIsfilled: " + isFilled() + "Color: " +  getColor() + "\n\n";
    }

    /**
     * Ham so sanh
     * @param obj oj can so sanh
     * @return T/F
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Square other = (Square) obj;
        return (this.getLayoutX() == other.getLayoutX() && this.getLayoutY() == other.getLayoutY() && this.getSide() == other.getSide());
    }
}
