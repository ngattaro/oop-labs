package OOP7;

import java.util.LinkedList;

/**
 * class Main khoi tao cac doi tuong
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-20
 */

public class Main
{
    public static void main (String[] args)
    {

        Shape shape1 = new Triangle(1.0,2.0,3.0,"blue",true,10,10);
        Shape shape2 = new Circle(10.5, "green", true,10,20);
        Shape shape3 = new Rectangle(2.9,7.1,"purple", true,30,10);
        Shape shape4 = new Square(5.0, "yellow", true,40,10);
        Shape shape5 = new Circle(10.5,"red",true, 10,20);
        Shape shape6 = new Circle(10.5,"brown",true, 10,20);
        Shape shape7 = new Rectangle(2.9,7.1,"red", true,30,10);
        Shape shape8 = new Square(5.0, "red", true,40,10);
        Shape shape9 = new Triangle(1.0,2.0,3.0,"red",true,10,10);
        Shape shape10 = new Hexagon(1.0,"yellow",true,10,10);
        Shape shape11 = new Hexagon(1.0,"red",true,10,10);
        LinkedList<Shape> list1 = new LinkedList<Shape>();
        list1.add(shape1);
        list1.add(shape2);
        list1.add(shape5);
        list1.add(shape6);
        list1.add(shape3);
        list1.add(shape4);
        list1.add(shape7);
        list1.add(shape8);
        list1.add(shape9);
        list1.add(shape10);
        list1.add(shape11);
        Layer layer1 = new Layer(list1,100,100);
        LinkedList<Layer> list2 = new LinkedList<Layer>();
       // System.out.println("Layer1:\n");
       // System.out.println(layer1.toString());
       // layer1.clearAllDuplicate();
        //System.out.println("Layer1:\n");
       // System.out.println(layer1.toString());

        Diagram diagram = new Diagram(list2,400,400);
        diagram.listLayers.add(layer1);
        System.out.println(diagram.toString());
        diagram.separateShape();
        System.out.println(diagram.toString());
    }
}
