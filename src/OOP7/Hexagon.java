package OOP7;
/**
 * class Hexagon extends Shape
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */
public class Hexagon extends Shape{
    private double side;

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
    /**
     * Ham khoi tao mac dinh
     */
    public Hexagon()
    {
        super();
        side = 1.0;
    }

    /**
     * Ham khoi tao canh
     * @param side canh
     */
    public Hexagon(double side)
    {
        super();
        setSide(side);
    }

    /**
     * Ham khoi tao day du thong so color, radius, filled
     * @param side canh
     * @param color0 mau cua hinh tron
     * @param filled0 trang thai fill
     * @param layoutX la toa do X
     * @param layoutY la toa do y
     */
    public Hexagon(double side, String color0, boolean filled0, int layoutX, int layoutY)
    {
        super(color0,filled0,layoutX,layoutY);
        setSide(side);
    }
    /**
     * Ham in thong tin
     * @return thong tin cua HV
     */
    @Override
    public String toString ()
    {
        return "Shape: Hexagon\nSide: " + getSide()+"\n"+ super.toString();
    }

    /**
     * Ham so sanh
     * @param obj oj can so sanh
     * @return T/F
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Hexagon other = (Hexagon) obj;
        return (this.getLayoutX() == other.getLayoutX() && this.getLayoutY() == other.getLayoutY() && this.getSide() == other.getSide());
    }
}
