package OOP7;
/**
 * class Triangle ke thua Class Shape
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */
public class Triangle extends Shape
{
    private double side1, side2, side3;

    public double getSide1 ()
    {
        return side1;
    }

    public double getSide2 ()
    {
        return side2;
    }

    public double getSide3 ()
    {
        return side3;
    }

    public void setSide1 (double side1)
    {
        this.side1 = side1;
    }

    public void setSide2 (double side2)
    {
        this.side2 = side2;
    }

    public void setSide3 (double side3)
    {
        this.side3 = side3;
    }
    /**
     * Ham khoi tao mac dinh
     */
    public Triangle()
    {
        super();
        side1 = 1.0;
        side2 = 1.0;
        side3 = 1.0;
    }

    /**
     * Ham khoi tao 3 canh
     * @param side1 canh thu nhat
     * @param side2 canh thu hai
     * @param side3 canh thu ba
     */
    public Triangle(double side1,double side2, double side3)
    {
        super();
        setSide1(side1);
        setSide2(side2);
        setSide3(side3);
    }

    /**
     * Ham khoi tao day du thong so color, radius, filled
     * @param side1 canh thu nhat
     * @param side2 canh thu hai
     * @param side3 canh thu ba
     * @param color0 mau cua hinh tron
     * @param filled0 trang thai fill
     * @param layoutX la toa do X
     * @param layoutY la toa do y
     */
    public Triangle(double side1,double side2, double side3, String color0, boolean filled0, int layoutX, int layoutY)
    {
        super(color0,filled0,layoutX,layoutY);
        setSide1(side1);
        setSide2(side2);
        setSide3(side3);
    }
    /**
     * Ham in thong tin
     * @return thong tin cua HV
     */
    @Override
    public String toString ()
    {
        return "Shape: Triangle\nSide: " + getSide1() +" "+getSide2()+" "+getSide3() +"\n"+ super.toString();
    }

    /**
     * Ham so sanh
     * @param obj oj can so sanh
     * @return T/F
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Triangle other = (Triangle) obj;
        return (this.getLayoutX() == other.getLayoutX() && this.getLayoutY() == other.getLayoutY() && this.getSide1() == other.getSide1() && this.getSide2() == other.getSide2() && this.getSide3() == other.getSide3());
    }
}
