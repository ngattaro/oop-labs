package OOP7;
/**
 * class Diagram
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-10-09
 */

import java.util.LinkedList;

public class Diagram
{
    protected double width, height;

    protected LinkedList<Layer> listLayers = new LinkedList<Layer>();

    public double getWidth ()
    {
        return width;
    }

    public double getHeight ()
    {
        return height;
    }

    public void setHeight (double height)
    {
        this.height = height;
    }

    public void setWidth (double width)
    {
        this.width = width;
    }

    /**
     * constructor rong
     */
    public Diagram(){};

    /**
     * constructor khoi tao chieu dai, rong
     * @param w chieu rong
     * @param h chieu dai
     */
    public Diagram(int w, int h)
    {
        setHeight(h);
        setWidth(w);
    }

    /**
     * constructor khoi tao nhieu layer
     * @param list danh sach layer
     * @param w chieu rong
     * @param h chieu dai
     */
    public Diagram(LinkedList<Layer> list, int w, int h)
    {
        setWidth(w);
        setHeight(h);
        listLayers.clear();
        listLayers.addAll(list);
    }

    /**
     * ham xoa tat ca doi tuong thuoc Retangle
     */
    public void removeAllTriangle()
    {
        for (Layer layer: listLayers)
        {
            for (Shape shape: layer.listShapes)
                if (shape instanceof Triangle) layer.listShapes.remove(shape);
        }
    }

    /**
     * Ham tach cac hinh thanh cac layer
     */
    public void separateShape()
    {
        LinkedList<Shape> listCircle = new LinkedList<Shape>();
        LinkedList<Shape> listSquare = new LinkedList<Shape>();
        LinkedList<Shape> listRetangle = new LinkedList<Shape>();
        LinkedList<Shape> listTriangle = new LinkedList<Shape>();
        LinkedList<Shape> listHexagon = new LinkedList<Shape>();

        for (int i = 0; i<listLayers.size(); i++)
        {
            for (Shape shape:listLayers.get(i).listShapes)
            {
                if(shape instanceof Circle) listCircle.add(shape);
                else if(shape instanceof Square) listSquare.add(shape);
                else if(shape instanceof Rectangle) listRetangle.add(shape);
                else if(shape instanceof Triangle) listTriangle.add(shape);
                else if(shape instanceof Hexagon) listHexagon.add(shape);
            }
        }
        int tmp = listLayers.size();
        for (int i = 0; i <5-tmp; i++) listLayers.add(new Layer());
        Layer layer0 = new Layer(listCircle);

        Layer layer1 = new Layer(listSquare);

        Layer layer2 = new Layer(listRetangle);

        Layer layer3 = new Layer(listTriangle);

        Layer layer4 = new Layer(listHexagon);

        listLayers.clear();
        if(listCircle != null) listLayers.add(layer0);
        if(listSquare != null) listLayers.add(layer1);
        if(listRetangle != null) listLayers.add(layer2);
        if(listTriangle != null) listLayers.add(layer3);
        if(listHexagon != null) listLayers.add(layer4);
    }

    /**
     * Ham in thong in
     * @return thong tin Diagram
     */
    @Override
    public String toString ()
    {
        String s = "Diagram \n\nWidth: " + getWidth() + " Height: " + getHeight() + "\n\nInclude:\n\n";
        for(int i = 0; i < listLayers.size(); i++)
            s += listLayers.get(i).toString();
        return s;
    }
}
