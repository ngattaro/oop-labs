package OOP4;

/**
 * chuong trinh Test cac ham tim max(a,b), min cua Array, tinh Bmi
 * @author Do Thi Hong Ngat
 * @version 1.0
 * @since 2018-09-20
 */

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OOP4Test
{
    OOP4 Ob = new OOP4();

    /**
     * ham test cho ham max(a,b) thu 1
     */
    @Test
    public void max1()
    {
        int x = Ob.max(10, 5);
        assertEquals(10, x);
    }
    /**
     * ham test cho ham max(a,b) thu 2
     */
    @Test
    public void max2()
    {
        int y = Ob.max(10, 7);
        assertEquals(10, y);
    }
    /**
     * ham test cho ham max(a,b) thu 3
     */
    @Test
    public void max3()
    {
        int y = Ob.max(10, 2);
        assertEquals(10, y);
    }
    /**
     * ham test cho ham max(a,b) thu 4
     */
    @Test
    public void max4()
    {
        int y = Ob.max(9, 0);
        assertEquals(9, y);
    }
    /**
     * ham test cho ham max(a,b) thu 5
     */
    @Test
    public void max5()
    {
        int y = Ob.max(100, -7);
        assertEquals(100, y);
    }
    /**
     * ham test cho ham minArr(n,a) thu 1
     */
    @Test
    public void minArr1()
    {
        int [] a = new int[]{3, 8, 1, 9, 3, 4};
        int x = Ob.minArr(6, a);
        assertEquals(1, x);
    }
    /**
     * ham test cho ham minArr(n,a) thu 2
     */
    @Test
    public void minArr2()
    {
        int [] a = new int[]{0, 3, 8, 1, 9, 3, 4};
        int x = Ob.minArr(7, a);
        assertEquals(0, x);
    }
    /**
     * ham test cho ham minArr(n,a) thu 3
     */
    @Test
    public void minArr3()
    {
        int [] a = new int[]{3, 8, 1, 10, -8, 9, 3, 4};
        int x = Ob.minArr(8, a);
        assertEquals(-8, x);
    }
    /**
     * ham test cho ham minArr(n,a) thu 4
     */
    @Test
    public void minArr4()
    {
        int [] a = new int[]{3, 8, 1, 9, 3, 4};
        int x = Ob.minArr(6, a);
        assertEquals(1, x);
    }
    /**
     * ham test cho ham minArr(n,a) thu 5
     */
    @Test
    public void minArr5()
    {
        int [] a = new int[]{-10, 3, 8, 1, 9, 3, 4};
        int x = Ob.minArr(7, a);
        assertEquals(-10, x);
    }
    /**
     * ham test cho ham Bmi(weight,height) thu 1
     */
    @Test
    public void checkBmi1()
    {
        String x = Ob.checkBmi(50,1.60);
        assertEquals("Binh thuong", x);
    }
    /**
     * ham test cho ham Bmi(weight,height) thu 2
     */
    @Test
    public void checkBmi2()
    {
        String x = Ob.checkBmi(49,1.60);
        assertEquals("Binh thuong", x);
    }
    /**
     * ham test cho ham Bmi(weight,height) thu 3
     */
    @Test
    public void checkBmi3()
    {
        String x = Ob.checkBmi(50.5,1.60);
        assertEquals("Binh thuong", x);
    }
    /**
     * ham test cho ham Bmi(weight,height) thu 4
     */
    @Test
    public void checkBmi4()
    {
        String x = Ob.checkBmi(50,1.63);
        assertEquals("Binh thuong", x);
    }
    /**
     * ham test cho ham Bmi(weight,height) thu 5
     */
    @Test
    public void checkBmi5()
    {
        String x = Ob.checkBmi(52,1.65);
        assertEquals("Binh thuong", x);
    }
}